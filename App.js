/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {Platform } from 'react-native';
import { Provider } from 'react-redux';
import { StackNavigator } from 'react-navigation';

import configureStore from './store/configureStore';
import MainScreen from './components/MainScreen';
import MenuScreen from "./components/MenuScreen";

const store = configureStore({});


const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});


const RootStack = StackNavigator(
    {
        Home: {
            screen: MainScreen,
        },
        Menu: {
            screen: MenuScreen,
        },/*
        Level: {
            screen: JustifyContentBasics,
        },*/
    },
    {
        initialRouteName: 'Home',
        headerMode: 'none',
        navigationOptions: {
            headerVisible: false,
        }
    },
);


export default class App extends React.Component {

  render() {
    return (
      <Provider store={store}>
        <RootStack />
      </Provider>
    )
  }
};
