const initialState = {
    initialized: false,
    user: {
        profile: null,
        checking: false,
        error: null,
    },
};

export const profile = (state = initialState, action) => {

    switch (action.type) {

        case 'SET_PROFILE':
            return {
                ...state,
                initialized: true,
                user: {
                    ...state.user,
                    checking: false,
                    profile: action.profile,
                },
            };

        default:
            return state
    }
};
