const initialState = {
  initialized: false,
  items: [
    { key: 1, id: 1, title: 'CHAMPIONS LEAGUE' },
    { key: 2, id: 2, title: 'WORLD CUP' },
    { key: 3, id: 3, title: 'EUROPE LEAGUE' },
    { key: 4, id: 4, title: 'ENGLISH CHAMPIONSHIP' },
    { key: 5, id: 5, title: 'TOP 50 HISTORY GOALS $', buy: true},
    { key: 6, id: 6, title: 'COPA AMERICA' },
    { key: 7, id: 6, title: 'BRAZIL' },
    { key: 8, id: 8, title: 'TOP 10 FUNNY GOALS' },
  ],
};

export const menu = (state = initialState, action) => {

  switch (action.type) {

    case 'SET_MENU':
      return {};

    default:
      return state
  }
};