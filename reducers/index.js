import { combineReducers } from 'redux';

import { profile } from './profile';
import { menu } from './menu';

export const rootReducer = combineReducers({
  profile,
  menu
});