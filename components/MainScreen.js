import React from 'react';
import { View, Text, StyleSheet } from 'react-native';


export default class MainScreen extends React.Component {

    render() {

        return (
            <View style={styles.container}>
                <Text
                    style={{color: '#ffffff', paddingTop: 10, fontWeight: 'bold',
                        fontFamily: 'Teko-Medium', fontSize: 40, lineHeight: 40}}
                    onPress={() => {
                        this.props.navigation.navigate('Menu', {
                            itemId: 86,
                        });
                    }}
                >{`WHO
SCORED
THE
GOAL?`}</Text>

                <Text
                    style={{color: '#ffffff', fontWeight: 'bold', textAlign: 'center',
                        fontFamily: 'Teko-Medium', fontSize: 40}}
                    onPress={() => {
                        this.props.navigation.navigate('Menu', {
                            itemId: 86,
                        });
                    }}
                >PLAY</Text>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
        backgroundColor: '#33032F',
        padding: 15,
        height: '100%'
    },
});