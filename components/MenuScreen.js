import React, { Component } from 'react';
import { Button, FlatList, StyleSheet, Text, View } from 'react-native';
import {connect} from "react-redux";

class MenuScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.props.menu.items.map(item=> {return{...item, key: item.key.toString()}})}
          renderItem={({ item }) => <Text
            onPress={() => {
              this.props.navigation.navigate('Level', {itemId: 0,});
            }}
            style={styles.item}>{item.title}</Text>}
        />
        <Button
          title="Go back"
          onPress={() => this.props.navigation.goBack()}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 22,
    backgroundColor: '#444554',
  },
  item: {
    padding: 10,
    fontSize: 22,
    height: 44,
    color: '#fff',
    fontWeight: 'bold',
    textAlign: 'center',
    fontFamily: 'Teko-Medium',
  },
});


const mapStateToProps = state => {
  const { menu } = state;
  return {
    menu
  }
};

const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(MenuScreen);